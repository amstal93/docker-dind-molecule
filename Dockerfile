FROM docker
RUN apk update && \
    apk add ansible build-base curl libc-dev libffi-dev openssl-dev python3 python3-dev py3-setuptools && \
    ln -s /usr/bin/python3 /usr/bin/python && \
    curl -s https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python3 get-pip.py && \
    pip install docker molecule --ignore-installed PyYAML
